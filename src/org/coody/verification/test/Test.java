package org.coody.verification.test;

import java.util.Date;

import org.coody.verification.process.ParamVerficationProcess;

public class Test {

	public static void main(String[] args) {
		
		OrderInfo order=new OrderInfo();
		order.setDate(new Date());
		order.setOrderId("20180531122355211");
		
		UserInfo userInfo=new UserInfo();
		//userInfo.setEmail("644556636@qq.com");
		userInfo.setMobile("123456");
		userInfo.setPassword("abc123456");
		userInfo.setUserName("Coody");
		order.setUserInfo(userInfo);
		
		ParamVerficationProcess.checkPara(order);
	}
}
