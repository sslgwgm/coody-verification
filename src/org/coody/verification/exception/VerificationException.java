package org.coody.verification.exception;

@SuppressWarnings("serial")
public class VerificationException extends RuntimeException{

	public VerificationException(String msg){
		super(msg);
	}
}
